﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CRUD.master" CodeBehind="Read.aspx.cs" Inherits="FinalProject.Read" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<h2 id="page_title" runat="server"></h2>
<asp:SqlDataSource runat="server"
    id="read_select"
    ConnectionString="<%$ ConnectionStrings:crud_final_con %>">
</asp:SqlDataSource>
<div id="read_query" runat="server" class="query_display">
<h1 id="title" runat="server"></h1>
<h2 id="date" runat="server"></h2>
<div id="content" runat="server"></div>

    </div>
    <!--Referenced your class and student pages for this code-->
    
    <asp:SqlDataSource 
        runat="server"
        id="del_page"
        ConnectionString="<%$ ConnectionStrings:crud_final_con %>">
    </asp:SqlDataSource>
    <asp:Button runat="server" id="del_page_btn"
        OnClick="DelPage"
        OnClientClick="if(!confirm('Are you sure?')) return false;"
        Text="Delete" />
    <div id="del_debug" class="querybox" runat="server"></div>
    
    <a href="EditPage.aspx?pageid=<%Response.Write(this.pageid);%>">Edit Content</a>

    
    <asp:DataGrid ID="read_list" runat="server">

    </asp:DataGrid>

</asp:Content>