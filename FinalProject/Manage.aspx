﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CRUD.master" CodeBehind="Manage.aspx.cs" Inherits="FinalProject.Manage" %>
 <asp:Content ContentPlaceHolderID="MainContent" runat="server">
<h2> Manage Pages </h2> 

<!--Based this off your students, classes, teachers example in the crud-basics solution.--> 

        <asp:SqlDataSource id="manage_select" runat="server"
        ConnectionString="<%$ ConnectionStrings:crud_final_con %>"
        ></asp:SqlDataSource>
        
        <div id="manage_query" class="query_display" runat="server"></div>
        
        <asp:DataGrid runat="server" id="CRUD_list">
        </asp:DataGrid>


</asp:Content>
