﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;


namespace FinalProject
{

    public partial class Menu : System.Web.UI.UserControl
    {
        //Based this off "Teacher_Pick" user control in your file but changed it from a drop down
        // list to a menu item, although this was where I started to hit a roadblock, as it wasn't 
        // recognizing certain pieces of code, however I think this may be a mac issue again.
        private string basequery = "SELECT pageid," +
            "pagename FROM CRUD_Pages";


        }
        protected void Page_Load(object sender, EventArgs e)
        {

             menu_pages.SelectCommand = basequery;

             Menu_Manual_Bind(menu_pages, "menu");
            
        }

       
        void Menu_Manual_Bind(SqlDataSource src, string menu_id)
        {
            


            Menu mymenu = (Menu)FindControl(menu_id);
            mymenu.Items.Clear();
            
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView row in myview)
            {
                
                MenuItem menu_item = new MenuItem();
                menu_item.Text = row["pagename"].ToString();
                menu_item.NavigateUrl = "~/Read.aspx?pageid="+row["pageid"];
                mymenu.Items.Add(menu_item);
            }
        }


    }
    


