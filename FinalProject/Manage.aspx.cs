﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace FinalProject
{ // Based this off your students, classes, teachers example in the crud-basics solution. 

    public partial class Manage : System.Web.UI.Page
    {
        private string sqlline = "SELECT pageid, pagetitle, pagedate as 'Date created'FROM CRUD_Pages"; 
        protected void Page_Load(object sender, EventArgs e)
        {
        manage_select.SelectCommand = sqlline;
        manage_query.InnerHtml = sqlline;
        CRUD_list.DataSource = Manage_Manual_Bind(manage_select);
        CRUD_list.DataBind();
        
        
        }
        protected DataView Manage_Manual_Bind(SqlDataSource src)
        {
            Console.Write("hi");
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["PAGETITLE"] =
                    "<a href=\"Read.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["PAGETITLE"]
                    + "</a>";
            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;
            
            return myview;

        }

    }
}
