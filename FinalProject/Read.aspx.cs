﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject
{ // Referenced your class and student pages for this code

    public partial class Read : System.Web.UI.Page
    
    {
        
            public string pageid {
            get { return Request.QueryString["pageid"]; 
                }
                }
            
       private string read_basequery = "SELECT pageid, pagetitle, pagecontent, pagedate FROM CRUD_Pages";
       
        protected void Page_Load(object sender, EventArgs e)
        {
            string pageid = Request.QueryString["pageid"];
            if(pageid ==""|| pageid ==null) page_title.InnerHtml = "No page with that name found.";
            else
            {
                read_basequery += " WHERE pageid = " + pageid;
                read_select.SelectCommand = read_basequery;
                //read_list.DataSource = read_select;
                //read_list.DataBind();
                
                DataView mypageview = (DataView)read_select.Select(DataSourceSelectArguments.Empty);
                DataRowView currentPageRow = mypageview[0];
                
                title.InnerHtml = currentPageRow["pagetitle"].ToString();
                date.InnerHtml = currentPageRow["pagedate"].ToString();
                content.InnerHtml = currentPageRow["pagecontent"].ToString();
                
                
                
            }
        } 
        protected void DelPage(object sender, EventArgs e)
        {
            string pageid = Request.QueryString["pageid"];
            string delquery = "DELETE FROM CRUD_Pages WHERE pageid="+ pageid;

            del_debug.InnerHtml = "This page has now been deleted!";
            del_page.DeleteCommand = delquery;
            del_page.Delete();

           
        }
    }
}
