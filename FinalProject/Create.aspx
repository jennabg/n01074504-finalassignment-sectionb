﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CRUD.master" CodeBehind="Create.aspx.cs" Inherits="FinalProject.Create" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<!--Based this off your New Class page from the crud-basics example-->

    <h2> Create a new content page </h2>
    <asp:SqlDataSource runat="server" id="create_page"
        ConnectionString="<%$ ConnectionStrings:crud_final_con %>">
     </asp:SqlDataSource>
     <div class="formrow">
        <label> Page Title: </label>
        <asp:TextBox id="NewPageTitle" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="NewPageTitle"
        ErrorMessage="Please enter a page title."></asp:RequiredFieldValidator>
     </div>
     <div class="formrow">
        <label> Page Content: </label><br />
        <asp:TextBox id="NewPageContent" runat="server" TextMode="MultiLine" Width="300px" Height="300"></asp:TextBox>
     </div>
     
     <ASP:Button Text="Create Page" runat="server" OnClick="CreatePage"/>
     <div runat="server" id="confirmmsg" ></div>
     
</asp:Content>