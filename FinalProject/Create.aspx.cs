﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;



namespace FinalProject
{ // Based this off your New Class page from the crud-basics example

    public partial class Create : System.Web.UI.Page
    {
        private string addquery = "INSERT INTO CRUD_Pages" +
        "(PAGEID, PAGETITLE, PAGECONTENT, PAGEDATE) VALUES";

    
    protected void Page_Load(object sender, EventArgs e)
    {
    
    }
    protected void CreatePage(object sender, EventArgs e)
    {
    string title = NewPageTitle.Text.ToString();
    string content = NewPageContent.Text.ToString();
    DateTime today = DateTime.Today.Date;
    
    addquery += "((select max(PAGEID) from CRUD_Pages)+1, '"+title+"','"+content+"', '"+today+"')";
    
    confirmmsg.InnerHtml = "Thank you, the page '" + title + "' has been created on '" + today +"'";
    
    create_page.InsertCommand = addquery;
    create_page.Insert();
       
    }
  }
}
