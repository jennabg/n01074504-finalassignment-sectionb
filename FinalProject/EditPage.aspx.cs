﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace FinalProject
{

    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }   
        protected void Page_Load(object sender, EventArgs e)
        {
            debug.InnerHtml="Ready!";
        }
     
      
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            //debug.InnerHtml="test123";
            DataRowView pagerow = getPageInfo(pageid);
            if(pagerow == null)
            {
                Update_Title.InnerHtml = "No Page Found.";
                return;
                
            }
            UpdatePageTitle.Text = pagerow["pagetitle"].ToString();
            UpdatePageContent.Text = pagerow["pagecontent"].ToString();
            Update_Title.InnerHtml = pagerow["pagetitle"].ToString();
        }
        
        protected void Update_Page(object sender, EventArgs e)
        {
            string title = UpdatePageTitle.Text;
            string content = UpdatePageContent.Text;
            
            string editquery = "Update CRUD_Pages set pagetitle = '"+title+"',"+
            "pagecontent='" +content+"'"+"where pageid="+pageid;
            
            update_page.UpdateCommand = editquery;
            update_page.Update();
        }
        
        protected DataRowView getPageInfo(int id)
        {
            string query ="SELECT * from CRUD_Pages where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;
            //debug.InnerHtml=query;
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            
            if(pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            
            DataRowView pagerowview = pageview[0];
            return pagerowview;
            
        }
        

    }
}
