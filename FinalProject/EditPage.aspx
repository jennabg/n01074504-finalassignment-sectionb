﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CRUD.master" CodeBehind="EditPage.aspx.cs" Inherits="FinalProject.EditPage" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="Update_Title"> Create a new content page </h3>
    <asp:SqlDataSource runat="server" id="update_page"
        ConnectionString="<%$ ConnectionStrings:crud_final_con %>">
     </asp:SqlDataSource>
    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:crud_final_con %>">
    </asp:SqlDataSource>
    
    <div class="inputrow">
        <label> Page Title: </label>
        <asp:TextBox id="UpdatePageTitle" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="UpdatePageTitle"
        ErrorMessage="Please enter a page title."></asp:RequiredFieldValidator>
     </div>
     <div class="formrow">
        <label> Page Content: </label><br />
        <asp:TextBox id="UpdatePageContent" runat="server" TextMode="MultiLine" Width="300px" Height="300"></asp:TextBox>
     </div>
     <div ID="debug" runat="server"></div>
     
     <ASP:Button Text="Update Page" runat="server" OnClick="Update_Page"/>
     
     
     <div runat="server" id="confirmmsg" ></div>
     
</asp:Content>